# DevOdds.com
Proyecto para mantener el sitio web institucional de la compañía. El sistio se implementa con una convinación de herramientas para poder editarlo y mantenerlo de forma simple. Implementa una plantilla de [***Hugo***](https://gohugo.io/) alojada en [***Netlify***](https://www.netlify.com/) y utiliando el administrador de contenido [***Forestry***](https://forestry.io/).

A continuación dejamos los pasos para el que quiera utilizar esta solución y agradecemos a la comunidad de [***Themefisher***](https://themefisher.com/) por  la excelente contribución.

## What you need !!

1. Git acccount (Ex: Github, Gitlab, etc). In our case we use ***Gitlab***.
2. [Netlify](https://bit.ly/netlify-account) account to host files and add custom domain .
3. [Forestry](https://bit.ly/forestry-account) account to maintain whole project without code.

### Step 1 : Fork or Clone repository

First we will fork this [meghna hugo](https://github.com/themefisher/meghna-hugo) template.

### Step 2 : Add your repository in Forestry

Go to your [forestry](https://bit.ly/forestry-account)  account and click on  `import your site now`. declare your config.toml file [`devodds.com`] and fill up basic settings .

**Or just click this button for one click installation** [![import to forestry](https://assets.forestry.io/import-to-forestryK.svg)](https://app.forestry.io/quick-start?repo=themefisher/meghna-hugo&engine=hugo&version=0.60.1&config=devodds.com)

Now mark everything as done, then go to configuration to change the base url.  You can put any url but this have to similar as netlify . So for now put a name  which you are going to put in netlify as netlify subdomain.

#### Step 3 : Setup and host website with Netlify

Here comes the last step . Go to your [netlify](https://bit.ly/netlify-account) account and click add new site . Choose your git repository to import your website in netlify.  
And now you can see the forked `meghna hugo` theme. select it and follow the steps. Then go to `site settings` for change the site name and put your subdoamin name here what you puted on forestry as base url. save it and go to `deploy` from top menu, Wait a while and click on `site preview` or just simply go to the 
subdomain you puted as base url. 

**BOOM! 💥 Your site is live.** Now you can go to forestry and add, remove or customize every setting and content.

> If you face any issue regarding the installation feel free to onen [open a new issue](https://github.com/themefisher/meghna-hugo/issues)

### Licensing

This Theme is released under [Creative Commons Attribution 3.0 (CC-BY-3.0) License](https://creativecommons.org/licenses/by/3.0/)
If you want to remove the credit simply make a [donation](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GSG5G2YL3E5V4), so that we can run our contribution to hugo community.

### Hire Us

We are available for Hiring of your next HUGO project. Drop Us a mail [themefisher@gmail.com](mailto:themefisher@gmail.com)

## Contribution

Para contribuir en el proyecto tener en cuenta lo definido [CONTRIBUTING.md](CONTRIBUTING.md)

Un punto a tener en cuenta cuando se trabaja sobre la rama de desarrollo es cambiar el valor de `baseURL` en el archivo [**config.toml**](devodds.com/config.toml) por `https://develop--devoddscom.netlify.com/` y regresarlo antes de incorporar los cambios a producción.

### Gestor de contenido CMS - Forestry

- **Acceso:** [Forestry](https://app.forestry.io/login)
- **Login:** con usuarios registrados (solicitar al administrador)
