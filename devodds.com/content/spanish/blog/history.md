---
title: Nuestra historia
date: 2020-04-10T05:00:00+00:00
author: David Broin
image_webp: "/images/company_history.png"
image: "/images/company_history.png"
description: This is meta description

---
Nuestra historia recién se va empezando a escribir, y así como esta web, poco a poco iremos consolidando lo que hoy es un sueño.

Emprendemos esta empresa con ganas de ver crecer nuestros proyectos, que serán los proyectos de nuestros clientes. Llevamos años aprendiendo y estamos convencidos de que llegó la hora de compartir nuestros conocimiento para colaborar con la solución tecnológica de problemáticas.

[**DevOdds**](https://devodds.com/) busca, en corto plazo, mejorar la experiencia que nuestros clientes tienen con su infraestructura. Creemos que hoy en día tener la infraestructura en la nube nos permite evolucionar de manera gradual y eso conlleva grandes ventajas desde lo administrativo, financiero y tecnológico.

Sin embargo, [**DevOdds**](https://devodds.com/) no termina ahí. [**DevOdds**](https://devodds.com/) es capaz de analizar los problemas de nuestros clientes y proponer soluciones que van desde la integración de _software_ y _hardware_ de terceros, hasta la implementación de _software_ y _hardware_ hecho completamente a la medida. En el medio, somos capaces de abordar el ciclo de vida completo de _software_, contemplando la operación, el mantenimiento y el monitoreo; así como también interconectar piezas de _software_ o ampliar aplicaciones a través de las más modernas tecnologías y metodologías.