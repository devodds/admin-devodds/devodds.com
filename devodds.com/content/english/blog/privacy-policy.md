+++
author = "Gabriel Lourenco"
date = 2022-09-29T05:00:00Z
description = "This is meta description"
image = "/images/privacy-policy.webp"
image_webp = "/images/privacy-policy.webp"
title = "Privacy policy"

+++
**PRIVACY POLICY**

We are committed to protect your information.

**_We will only use your data to improve your experience._**

**_You decide what and how you hear from us_**

We know you care about your privacy, so we are committed to protect your information as though it is our own. This **Policy aims** to help you understand what kind of information we might collect about you when you visit our site or when you use our system (and any of its links and sub-domains), for what purpose, and how we intend to use such information.

If you have any questions about our Privacy & Cookies Policy, contact us at [ventas@devodds.com](mailto:ventas@devodds.com)

**THE INFORMATION WE COLLECT**

Most of the personal information which we might collect about you is given to us only **if you choose** to give it to us. Such information may be requested from you when you fill in a field through website.

The information we collect from you normally includes name + surname, contact details (email address and telephone number), information about your device (phone or laptop) with which you use our systems, and any other personal information on documents you may upload.

Some other information is **given to us** because you accessed this website. This is explained in the Cookies section below.

**Legal Basis for Processing**

Under applicable data protection laws, there is a legal basis for us to use some of your personal data, sometimes, without having obtained your consent.

* This includes, for example, circumstances (such as we have described below) where we have a legitimate interest to use your data, provided that proper care is taken in relation to your rights and interests:
* to ensure that you know about any changes to the website or the terms of this Privacy Policy.
* to ensure that we organise our databases efficiently and understand how our clients may make purchases;
* to improve and ensure the security of the website (for example, for statistical, testing and analytical purposes, troubleshooting).

**Retention Periods**

* We will hold on to your information for no longer than is necessary keeping in mind the purpose/s (or compatible purposes) for which we first collected the data.
* We may also keep hold of some of your information if it becomes necessary or required to meet legal or regulatory requirements, resolve disputes, prevent fraud and abuse, or enforce our terms and conditions.

**COOKIES**

**What are cookies?**

A cookie is a small text file (typically numbers and letters) that is downloaded onto ‘terminal equipment’ (e.g. your computer or smartphone) when you (or someone else) access a website using that device. Cookies are then sent back to originating website on each subsequent visit – and they are useful because they allow a website to recognize a user’s device and store some information about your preferences or past actions.

Please note that the cookie will not identify you personally – only the browser that is installed on your computer, tablet or mobile that you use for the visit.

You can find our cookies policy here.

When you visit or access websites or applications operated by us or when you interact or engage with our services, we use cookies. These are placed on your device to allow us to automatically collect information about you and your online behaviour, as well as your device (for example your computer or mobile device), in order to enhance your navigation on our services.

**Disabling cookies**

You can enable or disable cookies through your browser even where you have accepted the use of cookies when first visiting our site. Please see [www.allaboutcookies.org](http://www.allaboutcookies.org) for more information.

**DISCLOSURE OF PERSONAL DATA**

**We do not, and will not, sell any of your personal data to any third party.** It is not our business to do so – and we want to earn your trust and confidence.

However, **we might share your data with** professional service providers who operate our business or companies in the group, as sometimes different companies within our group are responsible for different activities. Related entities and subsidiaries use the information collected to help us improve the content and functionality of our websites; to better understand our customers and markets; and to improve our products and services. Members of the group vary from time to time.

We will not normally disclose personal data, but **circumstances where we might be required to disclose personal information** without your consent include where there is a court order, to comply with legal requirements and satisfy a legal request, for the proper administration of justice, to protect your vital interests, to fulfil your requests, to safeguard the integrity of the relevant websites operated by us or by such related entities or subsidiaries, or in the event of a corporate sale, merger, reorganisation, dissolution or similar event involving us and/or the other entities within our group.

When we do share data, we do so on an understanding with the other entities that the data is to be used only for the purposes for which we originally intended.

**SECURITY OF YOUR PERSONAL DATA**

Security of your personal data is very important to us, so we do our best to keep the information you disclose to us secure. However, please understand no data transmission over the Internet is totally secure and no system can guarantee that unauthorised access or theft will not occur.

**CHANGES TO HOW WE PROTECT YOUR PRIVACY**

Our website is continually under review – new functions and features are periodically added and improved to interface, thus changes to our privacy policy may be required from time to time.

**LINKS TO OTHER WEBSITES**

This privacy notice does not cover the links within this site linking to other websites which are not controlled by us. We are not responsible for the collection or use of your personal information from these third-party websites. Therefore, we encourage you to read the privacy statements on the other websites you visit