# CONTRIBUTING
Está guia de contribución tiene como objetivo explicar los aspectos a tener en 
cuenta para poder realizar modificaciones en el proyecto.
Cualquier duda adicional consultar con los administradores del sitio.


### Iconos del proyecto
El proyecto implementa la librería de ìconos [***Themify***](https://themify.me/themify-icons)
por lo que cualquiera de estos se puede utilizar.
